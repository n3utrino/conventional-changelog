package ch.n3utrino.cchangelog

class Release(val version: String, val commits: List<Commit>, val name: String = "")
class Commit(val type: CommitType,
             val description: String,
             val scope: String = "",
             val body: String = "",
             val footer: String = "") {
    var breaking = body.contains("BREAKING CHANGE:")

}

enum class CommitType {
    FEATURE, FIX,
}