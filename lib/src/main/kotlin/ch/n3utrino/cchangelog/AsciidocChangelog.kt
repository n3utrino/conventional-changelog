package ch.n3utrino.cchangelog

import java.io.File

interface Changelog {
    fun writeChangelog(releases: List<Release>)
}

class AsciidocChangelog(private val changelogPath: String) : Changelog {

    override fun writeChangelog(releases: List<Release>) {

        val scopeGroup: (Commit) -> String = { commit ->
            when {
                commit.scope.isEmpty() -> "Various"
                else -> commit.scope.toLowerCase().capitalize()
            }
        }

        val changelogFile = File(changelogPath)
        changelogFile.delete()
        changelogFile.appendText("= Changelog\n\n")

        val writeCommit: (String, List<Commit>) -> Unit = fun(scope: String, commits: List<Commit>) {
            changelogFile.appendText("==== $scope\n\n")
            commits.forEach { commit -> changelogFile.appendText("* ${commit.description}\n") }
            changelogFile.appendText("\n")
        }

        for (release in releases.reversed()) {
            changelogFile.appendText("== ${release.version}\n\n")
            val breakingChanges = release.commits.filter { commit -> commit.breaking }
            writeBreakingChanges(breakingChanges, changelogFile, scopeGroup)
            val features = release.commits
                    .filter { commit -> commit.type.equals(CommitType.FEATURE) }
            if (features.isNotEmpty()) {
                changelogFile.appendText("=== Features\n\n")
                features
                        .groupBy { scopeGroup(it) }
                        .forEach { writeCommit(it.key, it.value) }
            }
            val fixes = release.commits
                    .filter { commit -> commit.type.equals(CommitType.FIX) }
            if (fixes.isNotEmpty()) {
                changelogFile.appendText("=== Fixes\n")
                fixes
                        .groupBy { scopeGroup(it) }
                        .forEach { writeCommit(it.key, it.value) }
            }
        }
    }

    private fun writeBreakingChanges(commits: List<Commit>, changelogFile: File, scopeGroup: (Commit) -> String) {
        if (commits.isNotEmpty()) {
            changelogFile.appendText("=== Breaking Changes\n\n")
            commits.groupBy { scopeGroup(it) }
                    .forEach { (scope, commits) ->
                        changelogFile.appendText("==== $scope\n\n")
                        changelogFile.appendText("|===\n")
                        changelogFile.appendText("|Change|Description\n\n")
                        commits.forEach { commit ->
                            changelogFile.appendText("|${commit.description}\n")
                            changelogFile.appendText("|${commit.body.replace("BREAKING CHANGE:", "")}\n\n")
                        }
                        changelogFile.appendText("|===\n\n")

                    }
        }
    }


}