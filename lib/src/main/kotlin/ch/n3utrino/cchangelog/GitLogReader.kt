package ch.n3utrino.cchangelog

import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.Constants
import org.eclipse.jgit.lib.Ref
import org.eclipse.jgit.lib.Repository
import org.eclipse.jgit.revwalk.RevCommit

class GitLogReader(private val repo: Repository,
                   private val versionTagRegex: String = "^([0-9]+)\\.([0-9]+)\\.([0-9]+)(?:-([0-9A-Za-z-]+(?:\\.[0-9A-Za-z-]+)*))?(?:\\+[0-9A-Za-z-]+)?\$") {

    private var git: Git = Git(repo)

    fun readLog(from: String?, to: String, path: String? = null): MutableIterable<RevCommit>? {

        val toObjectId = if (to == Constants.HEAD) {
            repo.resolve(Constants.HEAD)
        } else {
            val peeledTo = repo.refDatabase.peel(repo.exactRef(to))
            peeledTo.peeledObjectId ?: peeledTo.objectId
        }

        assert(toObjectId != null)

        val gitLogCommand = git.log()
        path?.let { gitLogCommand.addPath(path) }


        return when (from) {
            null -> gitLogCommand.add(toObjectId).call()
            else -> {
                val fromObject = repo.refDatabase.peel(repo.exactRef(from))
                val fromObjectId = fromObject.peeledObjectId ?: fromObject.objectId
                assert(fromObjectId != null)
                gitLogCommand
                        .addRange(fromObjectId, toObjectId)
                        .call()
            }
        }

    }

    fun readLog(path: String? = null, currentVersion: String = ""): Map<String, MutableIterable<RevCommit>?> {

        val tagCommitMap = mutableMapOf<String, MutableIterable<RevCommit>?>()

        val tags = git.tagList().call()
                .filter { ref -> ref.name.substring("/refs/tags/".length - 1).matches(versionTagRegex.toRegex()) }

        if (tags.isEmpty() && currentVersion.isEmpty()) {
            return tagCommitMap
        }

        var fromTag: Ref? = null
        var toTag: Ref?

        val iterator = tags.iterator()
        for (tag in iterator) {
            toTag = tag
            tagCommitMap[toTag.name] = readLog(fromTag?.name, toTag.name, path)
            fromTag = toTag
        }
        if (currentVersion.isNotEmpty()) {
            tagCommitMap[currentVersion] = readLog(to = Constants.HEAD, from = fromTag?.name, path = path)
        }

        return tagCommitMap

    }

}
