package ch.n3utrino.cchangelog

import org.eclipse.jgit.lib.RepositoryBuilder
import java.io.File

fun main() {
    ConventionalChangelog(
            File("./.git"),
            "./changelog.adoc").generateChangelog()
}

class ConventionalChangelog(gitDir: File, changelogFile: String, private val currentVersion: String = "") {

    private val gitLogReader = GitLogReader(RepositoryBuilder().setGitDir(gitDir).setMustExist(true).build())
    private val changeLogWriter = AsciidocChangelog(changelogFile)
    private val commitParser = CommitParser()

    fun generateChangelog() {
        val logs = gitLogReader.readLog(currentVersion = currentVersion)

        val releases = logs.map { versionCommits ->
            val messages = versionCommits.value?.map { revCommit -> revCommit.fullMessage }
            val version = versionCommits.key.replace("refs/tags/", "")

            when {
                messages != null -> Release(version, commitParser.parseCommits(messages))
                else -> Release(version, listOf())
            }

        }

        changeLogWriter.writeChangelog(releases)
    }


}