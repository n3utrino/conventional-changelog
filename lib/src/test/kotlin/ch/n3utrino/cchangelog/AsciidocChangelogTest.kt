package ch.n3utrino.cchangelog

import org.junit.Test

class AsciidocChangelogTest {

    @Test
    fun testWriteChangelog() {
        val writer = AsciidocChangelog("/tmp/changelog.adoc")

        val commits100 = listOf(
                Commit(CommitType.FIX, "Some Desc", "scope1", "BREAKING CHANGE: now everything is new and you should refrain from doing it the old way", "footer"),
                Commit(CommitType.FIX, "Some Desc", "Scope1", "body", "footer"),
                Commit(CommitType.FIX, "Some Desc No Scope"),
                Commit(CommitType.FEATURE, "Some Desc", "scope1", "BREAKING CHANGE: now everything is different and you need to change all the code", "footer"),
                Commit(CommitType.FEATURE, "Some Desc", "sCope2", "body", "footer"),
                Commit(CommitType.FEATURE, "Some Desc", "ScoPe2", "body", "footer"),
                Commit(CommitType.FEATURE, "Some Desc")
        )

        val release100 = Release("1.0.0", commits100)
        val release110 = Release("1.1.0", commits100)

        val releaseList = listOf(release100, release110)

        writer.writeChangelog(releaseList);
    }
}