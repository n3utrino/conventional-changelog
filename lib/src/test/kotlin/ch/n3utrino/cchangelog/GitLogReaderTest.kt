package ch.n3utrino.cchangelog

import org.eclipse.jgit.api.Git
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.io.File
import java.nio.file.Files
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertNotNull


internal class GitLogReaderTest {

    private lateinit var git: Git

    @Before
    fun setupRepo() {
        git = Git.init().setDirectory(File("/tmp/cc-gitTest-${Random.nextInt()}")).call()
        changeFile("feat(initial): initial test\n\nwith body\n\nand Footer")
        changeFile("chore: refactor")
        changeFile("fix: fix thing\n\nfixes the thing\n\nfixes #13")
        git.tag().setName("1.0.0").call()
        changeFile("chore: new version commit")
        changeFile("feat(Widget): next feature")
        git.tag().setName("someTagDoesNotMatch").call()
        changeFile("feat(Widget): big feature")
        changeFile("fix(Thingy): fix it\n\ndoes fix the broken thingy")
        git.tag().setName("1.1.0").call()
        changeFile("feat(Widget): another big feature for next release")

    }

    private fun changeFile(message: String) {
        File(git.repository.workTree.toString() + "/test.txt").writeText("Test${Random.nextInt()}")
        git.add().addFilepattern(".").call()
        git.commit().setMessage(message).call()
    }

    @After
    fun cleanup() {
        git.repository.workTree.deleteRecursively()
        Files.deleteIfExists(git.repository.workTree.toPath())
    }

    @Test
    fun testReadFullLog() {
        val logReader = GitLogReader(repo = git.repository)
        val versions = logReader.readLog("test.txt")

        assertNotNull(versions)
        assertEquals(2, versions.size)
        var parsedCommits = CommitParser().parseCommits(
                (versions["refs/tags/1.0.0"] ?: error("Version not found")).map { commit -> commit.fullMessage })
        assertEquals(2, parsedCommits.size)

        parsedCommits = CommitParser().parseCommits(
                (versions["refs/tags/1.1.0"] ?: error("Version not found")).map { commit -> commit.fullMessage })
        assertEquals(3, parsedCommits.size)

    }

    @Test
    fun testReadFullLogWithCurrentVersion() {
        val logReader = GitLogReader(repo = git.repository)
        val versions = logReader.readLog("test.txt",
                currentVersion = "1.2.3-SNAPSHOT")

        assertNotNull(versions)
        assertEquals(3, versions.size)
        var parsedCommits = CommitParser().parseCommits(
                (versions["refs/tags/1.0.0"] ?: error("Version not found")).map { commit -> commit.fullMessage })
        assertEquals(2, parsedCommits.size)

        parsedCommits = CommitParser().parseCommits(
                (versions["refs/tags/1.1.0"] ?: error("Version not found")).map { commit -> commit.fullMessage })
        assertEquals(3, parsedCommits.size)

        parsedCommits = CommitParser().parseCommits(
                (versions["1.2.3-SNAPSHOT"] ?: error("Version not found")).map { commit -> commit.fullMessage })
        assertEquals(1, parsedCommits.size)
    }

    @Test
    fun testReadLogFromTag() {
        val logReader = GitLogReader(repo = git.repository)

        val tags = git.tagList().call()
        val filteredTags = tags.filter { tag -> tag.name == "refs/tags/1.0.0" }
        assertNotNull(tags)
        assertEquals(1, filteredTags.size)
        val messages = logReader.readLog(from = "refs/tags/1.0.0", to = "refs/tags/1.1.0")?.map { commit -> commit.fullMessage }
        assertNotNull(messages)
        val parsedCommits = CommitParser().parseCommits(messages)
        assertEquals(3, parsedCommits.size)

    }
}