package ch.n3utrino.cchangelog

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertSame

class CommitParserTest {
    @Test
    fun testFull() {
        val classUnderTest = CommitParser()
        val commit = classUnderTest.parse("feat(widget): Add thingy to widget\n\nWidgets now work with sprockets\n\nFixes #13")
        assertSame(CommitType.FEATURE, commit.type, "feature should match")
        assertEquals("widget", commit.scope, "scope should match")
        assertEquals("Add thingy to widget", commit.description, "scope should match")
        assertEquals("Widgets now work with sprockets", commit.body, "body should match")
        assertEquals("Fixes #13", commit.footer, "footer should match")
    }

    @Test
    fun testFullSingleNewline() {
        val classUnderTest = CommitParser()
        val commit = classUnderTest.parse("feat(widget): Add thingy to widget\nWidgets now work with sprockets\nFixes #13")
        assertSame(CommitType.FEATURE, commit.type, "feature should match")
        assertEquals("widget", commit.scope, "scope should match")
        assertEquals("Add thingy to widget", commit.description, "scope should match")
        assertEquals("Widgets now work with sprockets", commit.body, "body should match")
        assertEquals("Fixes #13", commit.footer, "footer should match")
    }

    @Test
    fun testMinimal() {
        val classUnderTest = CommitParser()
        val commit = classUnderTest.parse("feat(widget): Add thingy to widget")
        assertSame(CommitType.FEATURE, commit.type, "feature should match")
        assertEquals("widget", commit.scope, "scope should match")

    }

    @Test
    fun testBody() {
        val classUnderTest = CommitParser()
        val commit = classUnderTest.parse("feat(widget): Add thingy to widget\n\nWidgets now work with sprockets")
        assertSame(CommitType.FEATURE, commit.type, "feature should match")
        assertEquals("widget", commit.scope, "scope should match")
        assertEquals("Widgets now work with sprockets", commit.body, "body should match")

    }

    @Test
    fun testDoesNotMatch() {
        val classUnderTest = CommitParser()
        assertFailsWith<IllegalStateException> {
            classUnderTest.parse("chore(widget): Add thingy to widget\n\nWidgets now work with sprockets")
        }


    }

    @Test
    fun commitList() {

        val commitStrings = listOf(
                "feat(widget): Add thingy to widget\n\nWidgets now work with sprockets\n\nFixes #13",
                "feat(widget): Add thingy to widget\nWidgets now work with sprockets\nFixes #13",
                "feat(widget): Add thingy to widget",
                "somecommit thingy",
                "chore(widget): Add thingy to widget\n\nWidgets now work with sprockets\n\nFixes #13",
                "feat(widget): Add thingy to widget\n\nWidgets now work with sprockets",
                "feat(widget): Add thingy to widget\nWidgets now work with sprockets"
        )
        val classUnderTest = CommitParser()
        val commits = classUnderTest.parseCommits(commitStrings)

        assertEquals(5, commits.size);

    }

}
